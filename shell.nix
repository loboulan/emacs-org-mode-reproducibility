{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    R
    emacs
    rPackages.ggplot2
    rPackages.ascii
    texlive.combined.scheme-full
  ];
}
