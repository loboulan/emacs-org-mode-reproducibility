EMACS=emacs
EMACS_FLAGS=-Q --batch

.PHONY: all
all: document.html document.pdf

document.html: document.org html.el
	$(EMACS) $(EMACS_FLAGS) document.org -l html.el

document.pdf: document.org pdf.el
	$(EMACS) $(EMACS_FLAGS) document.org -l pdf.el

.PHONY: clean
clean:
	rm -f *.pdf *.html *.tex *.png *.html~ *.tex~
